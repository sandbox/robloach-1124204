<?php

/**
 * Form builder; Creates a confirmation form to reset the user key.
 */
function user_key_reset_user_key_confirm($form, &$form_state, $account) {
  global $user;

  // Always provide entity id in the same form key as in the entity edit form.
  $form['_account'] = array('#type' => 'value', '#value' => $account);
  $form['uid'] = array('#type' => 'value', '#value' => $account->uid);

  // Prepare confirmation form page title and description.
  if ($account->uid == $user->uid) {
    $question = t('Are you sure you want to reset your user key?');
  }
  else {
    $question = t('Are you sure you want to reset the user key for %name?', array('%name' => $account->name));
  }

  // Display the confirmation form.
  return confirm_form($form,
    $question,
    'user/' . $account->uid,
    t('This action cannot be undone.'),
    t('Reset user key'), t('Cancel'));
}

/**
 * Submit handler; Handles when the user resets the user key.
 */
function user_key_reset_user_key_confirm_submit($form, &$form_state) {
  global $user;
  $account = $form_state['values']['_account'];
  // Confirm that the user has access.
  if (user_access('administer user key') || user_access('reset own user key') && $account->uid == $user->uid) {
    // Save the user account with a new user key.
    user_save($account, array(
      // Use the email, created date, and current time stamp.
      'user_key' => md5($user->mail . $user->created . time()),
    ));
    drupal_set_message(t("The user key has been reset."));
  }
  $form_state['redirect'] = "user/$account->uid";
}
